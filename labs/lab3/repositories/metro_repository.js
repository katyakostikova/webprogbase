const Metro = require('./../models/metro');
const JsonStorage = require('./../jsonStorage');
 
class MetroRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    getStations() {
    return this.storage.readItems().items; 
    }
    
    getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const stations = this.getStations();
        const stationsNumber = Number(stations.length);
        const offset = per_page * (page - 1);
        if (stationsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStations = [];
        let tempStationsLen = 0;
        if (name) {
            for (let i = 0; i < stations.length; i++) {
                if (stations[i].name.includes(name)) {
                    resStations.push(stations[i]);
                 }
            }
            tempStationsLen = resStations.length;
            resStations = resStations.slice(offset, offset + per_page);
        }
        const currentStations = stations.slice(offset, offset + per_page);
        let pagesNumber = 0;
        if ((stationsNumber / per_page) - Math.trunc(stationsNumber / per_page) != 0) {
            pagesNumber = Math.trunc(stationsNumber / per_page) + 1;
        }
        else {
            pagesNumber = Math.trunc(stationsNumber / per_page);
        }
        if (name) {
            if ((tempStationsLen / per_page) - Math.trunc(tempStationsLen / per_page) != 0) {
                pagesNumber = Math.trunc(tempStationsLen / per_page) + 1;
            }
            else {
                pagesNumber = Math.trunc(tempStationsLen / per_page);
            }
            if (pagesNumber == 0) {
                pagesNumber = 1;
            }
            return pagesNumber;
        }
        if (pagesNumber == 0) {
            pagesNumber = 1;
        }
        return pagesNumber;
    }
         
    getAllStations(page, per_page, name) { 
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const stations = this.getStations();
        const stationsNumber = Number(stations.length);
        const offset = per_page * (page - 1);
        if (stationsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStations = [];
        if (name) {
            for (let i = 0; i < stations.length; i++) {
                if (stations[i].name.includes(name)) {
                    resStations.push(stations[i]);
                }
            }
            resStations = resStations.slice(offset, offset + per_page);
        }
        const currentStations = stations.slice(offset, offset + per_page);
        if (name) {
            return resStations;
        }
        return currentStations;
    }
 
    getStationById(id) {
        const stations = this.storage.readItems();
        for (const item of stations.items) {
            if (item.id === id) {
                return new Metro(item.id, item.name, item.linecolor, item.numberOfExits, item.stationNumber, item.openDate, item.url);
            }
        }
        return null;
    }

    addStation(stationModel) {
        const station_id = this.storage.nextId;
        this.storage.incrementNextId();
        stationModel.id = station_id;
        const stations = this.storage.readItems();
        stations.items.push(stationModel);
        this.storage.writeItems(stations);
        return station_id;
    }
 
    updateStation(stationModel) {
        console.log(stationModel.id);
        const stations = this.storage.readItems();
        for (const [index, item] of stations.items.entries()) {
            console.log(item.id);
            if (item.id === stationModel.id) {
                stations.items[index] = stationModel;
                this.storage.writeItems(stations);
                return true;
            }
        } 
        return false;
    }
 
    deleteStation(stationId) {
        const stations = this.storage.readItems();
        for (const [index, item] of stations.items.entries()) {
            if (item.id === stationId) {
                stations.items.splice(index, 1);
                this.storage.writeItems(stations);
                return true;
            }
        }
        return false;
    }
};
 
module.exports  = MetroRepository;