const express = require('express');
const router = express.Router();
const mediaController = require('./../controllers/media');


router.post("/", mediaController.addMedia);

router.get("/:id", mediaController.getMediaById);


module.exports = router;