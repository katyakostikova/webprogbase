const bodyParser = require('body-parser');
const router = require('express').Router();
const userRouter = require('./users');
const metroRouter = require('./metro');
const mediaRouter = require('./media');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use('/users', userRouter);
router.use('/stations', metroRouter);
router.use('/media', mediaRouter);


module.exports = router;