const express = require('express');
const router = express.Router();
const metroController = require('../controllers/metro');

router.get("/new", (req, res) => { res.status(200).render('new') })

router.get("/:id", metroController.getStationById);

router.get("/", metroController.getStations);

router.post("/", metroController.postStation);

router.put("/", metroController.updateStation);

router.post("/:id", metroController.deleteStation);

module.exports = router;