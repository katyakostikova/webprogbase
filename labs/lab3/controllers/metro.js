const path = require('path');
const JsonStorage = require('../jsonStorage');
const Metro = require('../models/metro');
const MetroRepository = require('./../repositories/metro_repository');
const metroRepository = new MetroRepository(path.resolve(__dirname, '../data/metro.json'));
const jsonStorage = new JsonStorage(path.resolve(__dirname, '../data/metro.json'))
const fs = require("fs");

module.exports = {
    async getStations(req, res) {   
        try {
            stations = metroRepository.getAllStations(Number(req.query.page), Number(req.query.per_page), req.query.name);
            pagesNumber = metroRepository.getPagesNumber(Number(req.query.page), Number(req.query.per_page), req.query.name);
            let page = req.query.page;
            let name = req.query.name;
            if (!page) page = 1;
            else page = Number(page);
            pages = { currentPage: Number(page) }

            if (page != 1) pages.prevPage = page - 1;
            if (page != pagesNumber) pages.nextPage = page + 1; 
            if (name) pages.namePage = name;

            if (stations) {
                res.status(200).render('stations', {stations: stations, pagesNumber: pagesNumber, pages: pages});
            }
            else {
            res.status(404).send({stations: null, message: "Not found."});
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).send({stations: null, message: 'Server error.'}); 
        }
    },

    async getStationById(req, res) {
        try {
            const station = metroRepository.getStationById((parseInt(req.params.id)));
            if (station) {
                console.log(station);
                res.status(200).render("station", {station: station});
            }
            else {
                res.status(404).render({ message: "Not found"});
            }
        } catch(err) { 
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async postStation(req, res) {
        console.log(req.files);
        const fileFormat = req.files['url'].mimetype.split('/')[1];
        fs.writeFileSync(path.resolve(__dirname, '../data/media/' + jsonStorage.getNextId + '.' + fileFormat), req.files['url'].data, (err) => {
            if (err) {
                console.log("Error in loading photo");
            }
        })
        const url = '/media/' + jsonStorage.getNextId + '.' + fileFormat;
        const newStation = new Metro(jsonStorage.getNextId, req.body.name, req.body.linecolor, Number(req.body.numberOfExits), Number(req.body.stationNumber), req.body.openDate, url);
        const newId = metroRepository.addStation(newStation);
        res.redirect('/stations/' + newId);
    },

    async updateStation(req, res) {
        try {
            if(!req.body.id || !req.body.name || !req.body.linecolor || !req.body.numberOfExits || !req.body.stationNumber || ! req.body.openDate)
            {
                res.status(400).send({message: 'Bad Request'});
            }
            else
            {
                req.body.id = parseInt(req.body.id);
                req.body.numberOfExits = parseInt(req.body.numberOfExits);
                req.body.stationNumber = parseInt(req.body.stationNumber);
                if (metroRepository.updateStation(req.body))
                {
                    res.status(200).send({station: req.body, message: 'Station updated'});
                }
                else 
                {
                    res.status(404).send({stations: null, message: "Not found"});
                }
            }
        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async deleteStation(req, res)
    {
        try {
            if (metroRepository.deleteStation((parseInt(req.params.id)))) {
                res.redirect('/stations');
            }
            else {
                res.status(404).send({message: "Not found"});
            }

        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    }
};
