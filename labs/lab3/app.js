const express = require('express');
const app = express();
const path = require('path');
const mustache = require('mustache-express');
const body_parser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const morgan = require('morgan');

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

app.use(express.static('./public'));
app.use(express.static('./data'));

const mstRouter = require('./routes/api');

app.use(body_parser.urlencoded({ extended: true }));
app.use(body_parser.json());
app.use(busboyBodyParser());

app.use(morgan('dev'));

app.get('/', function(req, res) {
res.render('index');
});
app.get('/about', function(req, res) {
res.render('about');
});

app.use('', mstRouter);
app.use((req, res) => {
res.status(400).send({ message: "Incorrect routing."});
});

app.listen(3000, function() {
console.log('Server is ready');
});
