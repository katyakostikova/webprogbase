const express = require('express');
const router = express.Router({mergeParams:true});
const streetController = require('../controllers/streets');
const { merge } = require('./metro');

router.get("/:id", streetController.getStreetById);

router.get("/", streetController.getStreets);

router.post("/:id", streetController.deleteStreet);

router.post("/", streetController.postStreet);


module.exports = router;

