const bodyParser = require('body-parser');
const router = require('express').Router();
const userRouter = require('./users');
const streetsRouter = require('./streets');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use('/users', userRouter);
router.use('/streets', streetsRouter);


module.exports = router;