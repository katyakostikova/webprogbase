const express = require('express');
const router = express.Router({mergeParams:true});
const metroController = require('../controllers/metro');

router.get("/new", (req, res) => { res.status(200).render('new', {user_id: req.params.id }) });

router.get("/:id", metroController.getStationById);

router.get("/", metroController.getStations);

router.post("/", metroController.postStation);

router.put("/", metroController.updateStation);

router.post("/:id", metroController.deleteStation);

router.get("/:id/streets/newStreet", (req,res) => {res.status(200).render('newStreet', {station_id: req.params.id})});


module.exports = router;