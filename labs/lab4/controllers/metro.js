const MetroRepository = require('./../repositories/metro_repository');
const metroRepository = new MetroRepository();
const cloudinary = require('cloudinary');
const Street = require('../models/street');
cloudinary.config({
    cloud_name: "katyakostikova",
    api_key: 876818459361766,
    api_secret: "CF6CCGw2GQIMCKlclr3toJFVkws"
});

async function uploadRaw(buffer) {
    return new Promise((resolve, reject) => {
        cloudinary.v2.uploader
            .upload_stream({ resource_type: 'raw' },
                (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                })
            .end(buffer);
    });
}

module.exports = {
    async getStations(req, res) {   
        try {
            stations = await metroRepository.getAllStations(Number(req.query.page), Number(req.query.per_page), req.query.name, req.params.id);
            pagesNumber = await metroRepository.getPagesNumber(Number(req.query.page), Number(req.query.per_page), req.query.name, req.params.id);
            let page = req.query.page;
            let name = req.query.name;
            if (!page) page = 1;
            else page = Number(page);
            pages = { currentPage: Number(page) }

            if (page != 1) pages.prevPage = page - 1;
            if (page != pagesNumber) pages.nextPage = page + 1; 
            if (name) pages.namePage = name;
        

            if (stations) {
                res.status(200).render('stations', {stations: stations, pagesNumber: pagesNumber, pages: pages, user_id: req.params.id});
            }
            else {
            res.status(404).send({stations: null, message: "Not found."});
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).send({stations: null, message: 'Server error.'}); 
        }
    },

    async getStationById(req, res) {
        try {
            const station = await metroRepository.getStationById(req.params.id);
            if (station) {
                console.log(station);
                res.status(200).render("station", {station: station});
            }
            else {
                res.status(404).render({ message: "Not found"});
            }
        } catch(err) { 
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async postStation(req, res) {
        const result = await uploadRaw(req.files['url'].data);
        req.body.user_id = req.params.id;
        req.body.url = result.url;
        const newStationId = await metroRepository.addStation(req.body);
        res.redirect(`/users/${req.body.user_id}/stations/${newStationId}`);
    },

    async updateStation(req, res) {
        
    },

    async deleteStation(req, res)
    {
        try {
            await Street.deleteMany({station_id: req.params.id});
            station = await metroRepository.deleteStation(req.params.id);
            res.redirect('./');

        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    }
};
