const StreetRepository = require('../repositories/street_repository');
const MetroRepository = require('../repositories/metro_repository');
const streetRepository = new StreetRepository();
const metroRepository = new MetroRepository();

module.exports = {
    async getStreets(req, res) {
        try {
            streets = await streetRepository.getAllStreets(Number(req.query.page), Number(req.query.per_page), req.query.name);
            pagesNumber = await streetRepository.getPagesNumber(Number(req.query.page), Number(req.query.per_page), req.query.name);
            let page = req.query.page;
            let name = req.query.name;
            if (!page) page = 1;
            else page = Number(page);
            pages = { currentPage: Number(page) }

            if (page != 1) pages.prevPage = page - 1;
            if (page != pagesNumber) pages.nextPage = page + 1; 
            if (name) pages.namePage = name;

            if (streets) {
                res.status(200).render('streets', {streets: streets, pagesNumber: pagesNumber, pages: pages});
            }
            else {
            res.status(404).send({stations: null, message: "Not found."});
            }
        } catch (err) {
            console.log(err.message);
            res.status(500).send({stations: null, message: 'Server error.'}); 
        }
    },

    async getStreetById(req, res) {
        try {
            const street = await streetRepository.getStreetById(req.params.id);
            const station = await metroRepository.getStationById(street.station_id);
            if (street) {
                res.status(200).render("street", {street: street, station: station});
            }
            else {
                res.status(404).render({ message: "Not found"});
            }
        } catch(err) { 
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async postStreet(req, res) {
        const newStreetId = await streetRepository.addStreet(req.body);
        res.redirect(`/streets/${newStreetId}`);
    },

    async updateStreet(req, res) {
        
    },

    async deleteStreet(req, res)
    {
        try {
           street = await streetRepository.deleteStreet(req.params.id);
           res.redirect('/streets');

        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    }
};