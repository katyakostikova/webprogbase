const UserRepository = require('./../repositories/user_repository');
const userRepository = new UserRepository();

module.exports = {
    async getUsers(req, res) {
       try 
       {
            if(isNaN(Number(req.query.page)))
            {
                req.query.page = 1;
            }
            if(isNaN(Number(req.query.per_page)) || Number(req.query.per_page) > 5)
            {
                req.query.per_page = 2;
            }
            const users = await userRepository.getUsers(Number(req.query.page), Number(req.query.per_page));
            if(users)
            {
                console.log(users);
                res.status(200).render("users", {users: users});
            }
            else
            {
                res.status(404).render({ message: "Not found"});
            }
       }
       catch(err)
       {
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
       }
    },

    async getUserById(req, res) {
        try {
            
            const user = await userRepository.getUserById(req.params.id);

            if (user) {

                console.log(user);
                res.status(200).render("user", {user: user});

            }
            else {

                res.status(404).render({message: "Not found"});

            }

        } catch(err) {
            
            console.log(err.message);
            res.status(500).send({user: null, message: 'Server error'});
        }
    }
};