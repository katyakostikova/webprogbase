const express = require('express');
const router = express.Router();
const metroController = require('../controllers/metro');

/**
 * @route GET /api/metro/{id}
 * @group Stations - station operations
 * @param {integer} id.path.required - id of the Metro - eg: 1
 * @returns {Metro.model} 200 - Metro object
 * @returns {Error} 404 - Metro not found
 */
router.get("/:id", metroController.getStationById);


/**
 * @route GET /api/metro/
 * @group Stations - station operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Metro>} Photo - a page with photos
 */
router.get("/", metroController.getStations);

/**
 * @route POST /api/metro/
 * @group Stations - station operations
 * @param {Metro.model} id.body.required - new Metro object
 * @returns {Metro.model} 201 - added Metro object
 * @returns {Error} 400 - bad request
 */
router.post("/", metroController.postStation);

/**
 * @route PUT /api/metro/
 * @group Stations - station operations
 * @param {Metro.model} id.body.required - new Metro object
 * @returns {Metro.model} 200 - changed Metro object
 */
router.put("/", metroController.updateStation);

/**
 * @route DELETE /api/metro/{id}
 * @group Stations - station operations
 * @param {integer} id.path.required - id of the Metro - eg: 1
 * @returns {Metro.model} 200 - deleted Metro object
 * @returns {Error} 404 - Metro not found
 */
router.delete("/:id", metroController.deleteStation);

module.exports = router;