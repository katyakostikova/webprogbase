const express = require('express');
const apiRouter = require('./routes/api');
const expressSwaggerGenerator = require('express-swagger-generator');

const app = express();

const expressSwagger = expressSwaggerGenerator(app);
const options = {
    swaggerDefinition: {
        info: {
            description: 'Lab2',
            title: 'Lab2 swagger',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.use('/api', apiRouter);
    app.use((req, res) => {
        res.status(400).send({ message: "Incorrect routing1" });});
    
app.listen(3000, function() {console.log('Server is ready'); });






