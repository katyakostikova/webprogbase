const path = require('path');
const MetroRepository = require('./../repositories/metro_repository');
const metroRepository = new MetroRepository(path.resolve(__dirname, '../data/metro.json'));

module.exports = {
    async getStations(req, res) {
       try 
       {
            if(isNaN(req.query.page))
            {
                req.query.page = 1;
            }
            if(isNaN(req.query.per_page) || Number(req.query.per_page) > 100)
            {
                req.query.per_page = 2;
            }
            const stations = metroRepository.getAllStations(Number(req.query.page), Number(req.query.per_page));
            if(stations)
            {
                res.status(200).send({stations: stations, message: "Success"});
            }
            else
            {
                res.status(404).send({message: "Not found"});
            }
       }
       catch(err)
       {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
       }
    },

    async getStationById(req, res) {
        try {
            const station = metroRepository.getStationById((parseInt(req.params.id)));
            if (station) {
                console.log(station);
                res.status(200).send({stations: station, message: "Success"});
            }
            else {
                res.status(404).send({stations: null, message: "Not found"});
            }
        } catch(err) { 
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async postStation(req, res) {
        try {
            req.body.numberOfExits = parseInt(req.body.numberOfExits);
            req.body.stationNumber = parseInt(req.body.stationNumber);
            if(!req.body.name || !req.body.linecolor || !req.body.numberOfExits || !req.body.stationNumber || ! req.body.openDate)
            {
                res.status(400).send({message: 'Bad Request'});
            }
            else
            {
                metroRepository.addStation(req.body);
                res.status(201).send({station: req.body, message: 'Station created'});
            }
        } catch(err) {   
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async updateStation(req, res) {
        try {
            if(!req.body.id || !req.body.name || !req.body.linecolor || !req.body.numberOfExits || !req.body.stationNumber || ! req.body.openDate)
            {
                res.status(400).send({message: 'Bad Request'});
            }
            else
            {
                req.body.id = parseInt(req.body.id);
                req.body.numberOfExits = parseInt(req.body.numberOfExits);
                req.body.stationNumber = parseInt(req.body.stationNumber);
                if (metroRepository.updateStation(req.body))
                {
                    res.status(200).send({station: req.body, message: 'Station updated'});
                }
                else 
                {
                    res.status(404).send({stations: null, message: "Not found"});
                }
            }
        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    },

    async deleteStation(req, res)
    {
        try {
            if (metroRepository.deleteStation((parseInt(req.params.id)))) {
                res.status(200).send({message: "Success"});
            }
            else {
                res.status(404).send({stations: null, message: "Not found"});
            }

        } catch(err) {
            console.log(err.message);
            res.status(500).send({message: 'Server error'});
        }
    }
};