/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname.required - name and surname of user
 * @property {integer} role.required - if 1 - admin, if 0 - common user
 * @property {string} registeredAt.required - date of registration
 * @property {string} avaUrl - url picture 
 * @property {bool} isEnabled - true if user was activated
 */

class User {
    
    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role; // int number
        this.registeredAt = registeredAt; // date  ISO 8601.
        this.avaUrl = avaUrl; //рядок з URL зображення
        this.isEnabled = isEnabled; // відмітка чи користувача було активовано/деактивовано.
    }
 };
 
 module.exports = User;
 