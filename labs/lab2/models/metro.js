/**
 * @typedef Metro
 * @property {integer} id
 * @property {string} name.required - name of ther station
 * @property {string} linecolor.required - color on which line located 
 * @property {integer} numberOfExits.required - number of exits on this station
 * @property {integer} stationNumber.required - station number on line
 * @property {string} openDate.required - date of opening this station
 */

class Metro {
    
    constructor(id, name, linecolor, numberOfExits, stationNumber, openDate) 
    {
        this.id = id;  // number
        this.name = name;  // string
        this.linecolor = linecolor;  // string
        this.numberOfExits = numberOfExits; // int number
        this.stationNumber = stationNumber; //int number
        this.openDate = openDate; // date  ISO 8601.
    }
 };
 
 module.exports = Metro;