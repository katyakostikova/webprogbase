const fs = require("fs");
class JsonStorage {

    constructor(filePath) {
        return this._filepath = filePath;
    }

    get nextId() {
        const items = this.readItems();
        return items.nextId;
    }

    incrementNextId() {
        const items = this.readItems();
        items.nextId++;
        this.writeItems(items);
    }

    readItems() {
        const jsonText = fs.readFileSync(this._filepath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    writeItems(items) {
        const jsonText = JSON.stringify(items, null, 4);
        fs.writeFileSync(this._filepath, jsonText);
    }
};

module.exports = JsonStorage;