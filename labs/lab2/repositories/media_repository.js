const JsonStorage = require('./../jsonStorage');
const fs = require('fs');
 
class MediaRepository {
 
    constructor(filePath) {
        this.path= filePath;
        this.storage = new JsonStorage(filePath + ".json");
    }

    allFileFormats() {
        return this.storage.readItems().fileFormats;
    } 

    getMediaPath(mediaId) {
        for (const item of this.allFileFormats()) {
            const fullPath = this.path + '/' + String(mediaId) + '.' + item;
            if (fs.existsSync(fullPath)) {
                return fullPath;
            }
        }
        return null;
    }
};
 
module.exports  = MediaRepository;
