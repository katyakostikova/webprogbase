const User = require('./../models/user');
const JsonStorage = require('./../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers(page, per_page) { 
        const users = this.storage.readItems();
        return users.items.slice(((page-1)*per_page), ((page-1)*per_page)+per_page);
    }
 
    getUserById(id) {
        const users = this.storage.readItems();
        for (const item of users.items) {
            if (item.id === id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }

    addUser(userModel) {
        const user_id = this.storage.nextId;
        this.storage.incrementNextId();
        userModel.id = user_id;
        const users = this.storage.readItems();
        users.push(userModel);
        this.storage.writeItems(users);
        return user_id;
    }
 
    updateUser(userModel) {
        const users = this.storage.readItems();
        for (const [index, item] of users.items.entries()) {
            if (item.id === userModel.id) {
                users.items[index] = userModel;
                this.storage.writeItems(users);
                return true;
            }
        } 
        return false;
    }
 
    deleteUser(userId) {
        const users = this.storage.readItems();
        for (const [index, item] of users.items.entries()) {
            if (item.id === userId) {
                users.items.splice(index, 1);
                this.storage.writeItems(users);
                return true;
            }
        }
        return false;
    }
};
 
module.exports  = UserRepository;
