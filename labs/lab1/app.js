const UserRepository = require("./repositories/user_repository");
const MetroRepository = require("./repositories/metro_repository");
const readline = require('readline-sync');
const Metro = require("./models/metro");

function inputStationData(station)
{
    const name = readline.question("Enter station name:");
    name.trim();
    console.log(name + "\n");
    station.name = name;
    const line = readline.question("Enter station line:");
    line.trim();
    console.log(line + "\n");
    station.linecolor = line;
    while(true)
    {
        const numberOfExits = readline.question("Enter number of exits on this station:");
        numberOfExits.trim();
        console.log(numberOfExits + "\n");
        const number = parseInt(numberOfExits, 10);
        if(isNaN(number))
        {
            console.log("It's not a number, enter again.");
        }
        else
        {
            station.numberOfExits = number;
            break;
        }
    }
    while(true)
    {
        const stationNumber = readline.question("Enter station number: ");
        stationNumber.trim();
        console.log(stationNumber + "\n");
        const number = parseInt(stationNumber, 10);
        if(isNaN(number))
        {
            console.log("It's not a number, enter again.");
        }
        else
        {
            station.stationNumber = number;
            break;
        }
    }
    while(true)
    {
        const openDate = readline.question("Enter open date (ex: 2020-10-07):");
        openDate.trim();
        console.log(openDate + "\n");
        const numbers = openDate.split('-');
        const year = parseInt(numbers[0], 10);
        const month = parseInt(numbers[1], 10);
        const day = parseInt(numbers[2], 10);
        if((isNaN(year) === false) && ((isNaN(month) === false) && month > 0 && month < 13) && (isNaN(day) === false) && day > 0 && day < 32)
        {
            const date = new Date(year, month-1, day+1);
            const dateString = date.toISOString();
            station.openDate = dateString;
            break;
        }
        else
        {
            console.log("Wrong data input, try again");
        }
    }
    return station;
}

while (true) 
{
    const input = readline.question("Enter command:");
    input.trim();
    console.log(input + "\n");

    if (input === "get/users") 
    {
        const userRep = new UserRepository("./data/users.json");
        const users = userRep.getUsers();
        for(const item of users)
        {
            console.log("Id: " + item.id + " Login: " + item.login);
        }
    }
    else if(input.includes("get/users/"))
    {
        const userRep = new UserRepository("./data/users.json");
        const index = parseInt(input.substring(input.lastIndexOf('/')+1), 10);
        const user = userRep.getUserById(index);
        if(user !== null)
        {
            console.log("Login: " + user.login + "\nFullname" + user.fullname );
            if(user.role === 1)
            {
                console.log("Role: Admin");
            }
            else
            {
                console.log("Role: Common user");
            }
            const date = new Date(user.registeredAt);
            console.log("Registered at: " + date + "\n");
        }
        else
        {
            console.log("Wrong id or user does not exist");
        }
    }
    else if(input === "get/stations")
    {
        const metroRep = new MetroRepository("./data/metro.json");
        const stations = metroRep.getAllStations();
        for(const item of stations)
        {
            console.log("Id: " + item.id + " Name: " + item.name);
        }
    }
    else if(input.includes("get/stations/"))
    {
        const metroRep = new MetroRepository("./data/metro.json");
        const index = parseInt(input.substring(input.lastIndexOf('/')+1), 10);
        const station = metroRep.getStationById(index);
        if(station !== null)
        {
            console.log("Name: " + station.name + "\nLine color: " + station.linecolor + "\nNamber of exits: " + station.numberOfExits + "\nStation number: " + station.stationNumber);
            const date = new Date(station.openDate);
            console.log("Opened at: " + date + "\n");
        }
        else
        {
            console.log("Wrong id or station does not exist");
        }
    }
    else if(input.includes("delete/stations/"))
    {
        const metroRep = new MetroRepository("./data/metro.json");
        const index = parseInt(input.substring(input.lastIndexOf('/')+1), 10);
        const isDeleted = metroRep.deleteStation(index);
        if(isDeleted)
        {
            console.log("Station was deleted successfully");
        }
        else
        {
            console.log("Wrong id or station does not exist");
        }
    }
    else if(input.includes("update/stations/"))
    {
        const metroRep = new MetroRepository("./data/metro.json");
        const index = parseInt(input.substring(input.lastIndexOf('/')+1), 10);
        let station = metroRep.getStationById(index);
        if(station !== null)
        {
            station = inputStationData(station);
            const isUpdated = metroRep.updateStation(station);
            if(isUpdated)
            {
                console.log("Station was updated successfully");
            }
            else
            {
                console.log("Something went wrong");
            }
        }
        else
        {
            console.log("Wrong id or station does not exist");
        }
    }
    else if(input === "post/stations")
    {
        const metroRep = new MetroRepository("./data/metro.json");
        let station = new Metro;
        station = inputStationData(station);
        const station_id = metroRep.addStation(station);
        console.log("Station was added successfully, id: " + station_id);
    }
    else if(input === "")
    {
        process.exit(0);
    }
    else
    {
        console.log("Wrong command\n");
    }
}
