const Metro = require('./../models/metro');
const JsonStorage = require('./../jsonStorage');
 
class MetroRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getAllStations() { 
        const stations = this.storage.readItems();
        return stations.items;
    }
 
    getStationById(id) {
        const stations = this.storage.readItems();
        for (const item of stations.items) {
            if (item.id === id) {
                return new Metro(item.id, item.name, item.linecolor, item.numberOfExits, item.stationNumber, item.openDate);
            }
        }
        return null;
    }

    addStation(stationModel) {
        const station_id = this.storage.nextId;
        this.storage.incrementNextId();
        stationModel.id = station_id;
        const stations = this.storage.readItems();
        stations.items.push(stationModel);
        this.storage.writeItems(stations);
        return station_id;
    }
 
    updateStation(stationModel) {
        const stations = this.storage.readItems();
        for (const [index, item] of stations.items.entries()) {
            if (item.id === stationModel.id) {
                stations.items[index] = stationModel;
                this.storage.writeItems(stations);
                return true;
            }
        } 
        return false;
    }
 
    deleteStation(stationId) {
        const stations = this.storage.readItems();
        for (const [index, item] of stations.items.entries()) {
            if (item.id === stationId) {
                stations.items.splice(index, 1);
                this.storage.writeItems(stations);
                return true;
            }
        }
        return false;
    }
};
 
module.exports  = MetroRepository;