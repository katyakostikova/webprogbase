class Metro {
    
    constructor(id, name, linecolor, numberOfExits, stationNumber, openDate) 
    {
        this.id = id;  // number
        this.name = name;  // string
        this.linecolor = linecolor;  // string
        this.numberOfExits = numberOfExits; // int number
        this.stationNumber = stationNumber; //int number
        this.openDate = openDate; // date  ISO 8601.
    }
 };
 
 module.exports = Metro;