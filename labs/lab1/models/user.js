class User {
    
    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role; // int number
        this.registeredAt = registeredAt; // date  ISO 8601.
        this.avaUrl = avaUrl; //рядок з URL зображення
        this.isEnabled = isEnabled; // відмітка чи користувача було активовано/деактивовано.
    }
 };
 
 module.exports = User;
 