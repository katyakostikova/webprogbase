const Metro = require('./../models/metro');
 
class MetroRepository {
 
    async getStations() {
        const stations = await Metro.find();
        const jsonDoc = stations.map(x => x.toJSON());
        return jsonDoc;
    }
    
    async getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const stations = await this.getStations();
        const stationsNumber = Number(stations.length);
        const offset = per_page * (page - 1);
        if (stationsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStations = [];
        let tempStationsLen = 0;
        if (name) {
            for (let i = 0; i < stations.length; i++) {
                if (stations[i].name.includes(name)) {
                    resStations.push(stations[i]);
                 }
            }
            tempStationsLen = resStations.length;
            resStations = resStations.slice(offset, offset + per_page);
        }
        const currentStations = stations.slice(offset, offset + per_page);
        let pagesNumber = 0;
        if ((stationsNumber / per_page) - Math.trunc(stationsNumber / per_page) != 0) {
            pagesNumber = Math.trunc(stationsNumber / per_page) + 1;
        }
        else {
            pagesNumber = Math.trunc(stationsNumber / per_page);
        }
        if (name) {
            if ((tempStationsLen / per_page) - Math.trunc(tempStationsLen / per_page) != 0) {
                pagesNumber = Math.trunc(tempStationsLen / per_page) + 1;
            }
            else {
                pagesNumber = Math.trunc(tempStationsLen / per_page);
            }
            if (pagesNumber == 0) {
                pagesNumber = 1;
            }
            return pagesNumber;
        }
        if (pagesNumber == 0) {
            pagesNumber = 1;
        }
        return pagesNumber;
    }
         
    async getAllStations(page, per_page, name) { 
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const stations = await this.getStations();
        const stationsNumber = Number(stations.length);
        const offset = per_page * (page - 1);
        if (stationsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStations = [];
        if (name) {
            for (let i = 0; i < stations.length; i++) {
                if (stations[i].name.includes(name)) {
                    resStations.push(stations[i]);
                }
            }
            resStations = resStations.slice(offset, offset + per_page);
        }
        const currentStations = stations.slice(offset, offset + per_page);
        if (name) {
            return resStations;
        }
        return currentStations;
    }
 
    async getStationById(id) {
        const items = await Metro.findById(id);
        return items.toJSON();
    }

    async addStation(stationModel) {
        console.log(stationModel)
        const st = new Metro(stationModel);
        await Metro.insertMany(st);
        return st._id
    }
 
    async deleteStation(stationId) {
        await Metro.deleteOne({_id: stationId});
    }

    async getLastStations() {
        const stationsDocs = await Metro.find().sort({'openDate': -1}).limit(3);
        return stationsDocs.map(stations => stations.toJSON());

    }
};
 
module.exports  = MetroRepository;