const User = require('./../models/user');

class UserRepository {

    async getUsers(page, per_page) { 
        const users = await User.find();
        const jsonDoc = users.map(x => x.toJSON());
        return jsonDoc.slice(((page-1)*per_page), ((page-1)*per_page)+per_page);
    }

    async getUserById(id) {
        const user = await User.findById(id);
        return user;
    }
};

module.exports  = UserRepository;
