const Street = require('./../models/street');
 
class StreetRepository {
 
    async getStreets() {
        const streets = await Street.find();
        const jsonDoc = streets.map(x => x.toJSON());
        return jsonDoc;
    }
    
    async getPagesNumber(page, per_page, name) {
        const page_size = 3;
        const maxPageSize = 3;
        
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const streets = await this.getStreets();
        const streetsNumber = Number(streets.length);
        const offset = per_page * (page - 1);
        if (streetsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStreets = [];
        let tempStreetsLen = 0;
        if (name) {
            for (let i = 0; i < streets.length; i++) {
                if (streets[i].name.includes(name)) {
                    resStreets.push(streets[i]);
                 }
            }
            tempStreetsLen = resStreets.length;
            resStreets = resStreets.slice(offset, offset + per_page);
        }
        const currentStreets = streets.slice(offset, offset + per_page);
        let pagesNumber = 0;
        if ((streetsNumber / per_page) - Math.trunc(streetsNumber / per_page) != 0) {
            pagesNumber = Math.trunc(streetsNumber / per_page) + 1;
        }
        else {
            pagesNumber = Math.trunc(streetsNumber / per_page);
        }
        if (name) {
            if ((tempStreetsLen / per_page) - Math.trunc(tempStreetsLen / per_page) != 0) {
                pagesNumber = Math.trunc(tempStreetsLen / per_page) + 1;
            }
            else {
                pagesNumber = Math.trunc(tempStreetsLen / per_page);
            }
            if (pagesNumber == 0) {
                pagesNumber = 1;
            }
            return pagesNumber;
        }
        if (pagesNumber == 0) {
            pagesNumber = 1;
        }
        return pagesNumber;
    }
         
    async getAllStreets(page, per_page, name) { 
        const page_size = 3;
        const maxPageSize = 3;
        if (per_page) {
            if (per_page > maxPageSize) {
                console.log("Error.");
                return undefined;
            }
        }
        else {
            per_page = page_size;
        }
        if (!page) {
            page = 1;
        }
        const streets = await this.getStreets();
        const streetsNumber = Number(streets.length);
        const offset = per_page * (page - 1);
        if (streetsNumber <= offset) {
            console.log("Error.");
            return undefined;
        }
        let resStreets = [];
        if (name) {
            for (let i = 0; i < streets.length; i++) {
                if (streets[i].name.includes(name)) {
                    resStreets.push(streets[i]);
                }
            }
            resStreets = resStreets.slice(offset, offset + per_page);
        }
        const currentStreets = streets.slice(offset, offset + per_page);
        if (name) {
            return resStreets;
        }
        return currentStreets;
    }
 
    async getStreetById(id) {
        const items = await Street.findById(id);
        return items.toJSON();
    }

    async addStreet(streetModel) {
        const st = new Street(streetModel);
        await Street.insertMany(st);
        return st._id
    }
 
    async deleteStreet(streetId) {
        await Street.deleteOne({_id: streetId});
    }
};
 
module.exports  = StreetRepository;