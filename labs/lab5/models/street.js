const mongoose = require('mongoose');

const StreetSchema = new mongoose.Schema({
    name: { type: String, required: true },
    station_id: { type: mongoose.Types.ObjectId, ref: 'Metro', required: true }
}, {collection : 'streets'});

const StreetModel = mongoose.model('Street', StreetSchema);
 
module.exports = StreetModel;