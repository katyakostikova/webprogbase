const mongoose = require('mongoose');

const MetroSchema = new mongoose.Schema({
    name: { type: String, required: true },
    linecolor: { type: String, required: true },
    numberOfExits: { type: Number, required: true },
    stationNumber: { type: Number, required: true },
    openDate: { type: String, default: Date.now },
    url: { type: String },
    // user_id: { type: mongoose.Types.ObjectId, ref: 'User', required: true }
}, {collection : 'stations'});

const MetroModel = mongoose.model('Metro', MetroSchema);

module.exports = MetroModel;