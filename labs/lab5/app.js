const express = require('express');
const app = express();
const path = require('path');
const mustache = require('mustache-express');
const body_parser = require('body-parser');
const busboyBodyParser = require('busboy-body-parser');
const morgan = require('morgan');
const mongoose = require("mongoose");
require("dotenv").config();
const mainRouter = require('./routes/main');


const dbUrl = process.env.DBNAME;
const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};

const port = process.env.PORT || 3000;

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

const http = require("http");
const ws = require("ws");
let webSocketConnections = require("./utils/webSocketCollection").socketCollection;

const server = http.createServer(app);

const wsServer = new ws.Server({ server });

wsServer.on("connection", (connection) => {
    console.log(webSocketConnections);
    webSocketConnections.push(connection);
    console.log("(+) new connection. total connections:", webSocketConnections.length);

    connection.on("close", () => {
        webSocketConnections.splice(webSocketConnections.indexOf(connection), 1);
        console.log(
            "(-) connection lost. total connections:",
            webSocketConnections.length
        );
    });
});

app.use(express.static('./data'));
app.use(express.static('./public'));


app.use(body_parser.urlencoded({ extended: true }));
app.use(body_parser.json());
app.use(busboyBodyParser());

app.use(morgan('dev'));

app.get('/', function(req, res) {
res.render('index');
});
app.get('/about', function(req, res) {
res.render('about');
});

app.use('', mainRouter);
app.use((req, res) => {
res.status(400).send({ message: "Incorrect routing."});
});

server.listen(port, async () => {
   try {
        console.log(`Server ready`);
        const client = await mongoose.connect(dbUrl, connectOptions);
        console.log('Mongo database connected');
   } catch (error) {
        console.log(error);
   }
});
