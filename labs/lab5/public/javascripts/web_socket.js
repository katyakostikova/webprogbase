let stationToastStr = "";
const protocol = location.protocol === "https:" ? "wss:" : "ws:";
const wsLocation = `${protocol}//${location.host}`;
const connection = new WebSocket(wsLocation);

document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/toast.mst").then((x) => x.text())
        .then((stationToast) => {
            stationToastStr = stationToast;
        })
        .catch((err) => console.error(err));
})

function showToastMessage(messageText) {
    const newItem = JSON.parse(messageText.data);
    const appEl = document.getElementById("push-toasts");
    const date = new Date();
    const time = date.getHours() + ":" + date.getMinutes();
    appEl.insertAdjacentHTML('beforeend', Mustache.render(stationToastStr, {
        id: newItem._id,
        name: newItem.name,
        date: time
    }));
    const toastEl = document.getElementById("liveToast" + newItem._id);
    const newItemHref = document.getElementById("newItem" + newItem._id);
    const btnX = document.getElementById("buttonX" + newItem._id);
    btnX.addEventListener('click', () => {
        document.getElementById("liveToast" + newItem._id).classList.add("hide");
    });
    newItemHref.href = `/stations/${newItem._id}`;
    const toastOption = {
        animation: true,
        autohide: true,
        delay: 7000,
    };
    const toast1 = new bootstrap.Toast(toastEl, toastOption);
    toast1.show();
}
connection.addEventListener("open", () =>
    console.log("Connected to ws server:", wsLocation)
);
connection.addEventListener("error", () => console.error('ws error'));
connection.addEventListener("message", (message) => {
    showToastMessage(message);
});