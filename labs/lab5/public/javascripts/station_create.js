let stationsLoadingStr = "";
let stationsLastStr = "";

document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/last_stations.mst").then((x) => x.text())
        .then((stationsLast) => {
            stationsLastStr = stationsLast;
        })
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst").then((x) => x.text())
        .then((loading) => {
            stationsLoadingStr = loading;
        })
        .catch((err) => console.error(err));
})

function uploadData() {
    const appEl = document.getElementById("divNewRightPart");
    appEl.innerHTML = stationsLoadingStr;
    fetch("/api/stations").then((x) => x.json())
        .then((allStations) => {
            const renderedHtmlStrList = Mustache.render(stationsLastStr, {
                stations: allStations.lastStations,
            });
            return renderedHtmlStrList;
        })
        .then((htmlStr) => {
            const appEl = document.getElementById("divNewRightPart");
            appEl.innerHTML = htmlStr;
            const newStationButton = document.getElementById("SubmitButton");
            const currentForm = document.getElementById("formCreationStation");
            newStationButton.disabled = false;

            newStationButton.onclick = () => {
                currentForm.onsubmit = function (el) {
                    el.preventDefault();
                    const formData = new FormData(this);
                    postData(formData);
                    currentForm.reset();
                }
            };
        })
        .catch((err) => console.error(err));
}

function postData(formData) {
    const newStationButton = document.getElementById("SubmitButton");
    const currentForm = document.getElementById("formCreationStation");
    newStationButton.removeAttribute("onclick");
    currentForm.removeAttribute("onsubmit");
    
    const appEl = document.getElementById("divNewRightPart");
    newStationButton.disabled = true;
    appEl.innerHTML = stationsLoadingStr;

    fetch("/api/stations", {
        method: "POST",
        body: formData,
    }).then((x) => x.json())
    .then((x) => uploadData())
    .catch((err) => console.error(err));
    
}

uploadData();