let stationsTableStr = "";
let stationsPaginationStr = "";
let loadingStr = "";
document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/stations_table.mst").then((x) => x.text())
        .then((stationsTable) => {
            stationsTableStr = stationsTable;
        })
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst").then((x) => x.text())
        .then((loading) => {
            loadingStr = loading;
        })
        .catch((err) => console.error(err));
    fetch("/templates/pagination.mst").then((x) => x.text())
        .then((stationsPagination) => {
            stationsPaginationStr = stationsPagination;
        })
        .catch((err) => console.error(err));
})

function uploadData(currentPage, pageName) {
    const loadPage = document.getElementById("content");
    loadPage.innerHTML = loadingStr;
    fetch(`/api/stations?page=${currentPage}&name=${pageName}`).then((x) => x.json())
        .then((stationData) => {
            const renderedPaginationStr = Mustache.render(stationsPaginationStr, {
                pagesNumber: stationData.pagesNumber,
                currentPage: stationData.pages.currentPage,
                namePage: pageName
            });
            const renderedHtmlStr = Mustache.render(stationsTableStr, {
                stations: stationData.stations,
                pagination: renderedPaginationStr
            });
            return renderedHtmlStr;
        })
        .then((htmlStr) => {
            const appEl = document.getElementById("content");
            appEl.innerHTML = htmlStr;
            const prev = document.getElementById("prevPage");
            const next = document.getElementById("nextPage");
            const nameStationInput = document.getElementById("nameStationInputId");
            const pageNumberId = document.getElementById("pageNumberId");
            if (currentPage == 1) {
                prev.classList.add("disableButton");
            }
            if (currentPage == Number.parseInt(pageNumberId.textContent)) {
                next.classList.add("disableButton");
            }
            nameStationInput.oninput = () => {
                uploadData(1, nameStationInput.value);
            };
            prev.addEventListener('click', () => {
                uploadData(currentPage - 1, pageName);
            })
            next.addEventListener('click', () => {
                uploadData(currentPage + 1, pageName);
            })
        })
        .catch((err) => console.error(err));
}

uploadData(1, "");