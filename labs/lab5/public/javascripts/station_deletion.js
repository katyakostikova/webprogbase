let loadingStr = "";
let stationsLoadingStr = "";
let stationsDeleteStr = "";

document.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed');
    fetch("/templates/station_delete.mst").then((x) => x.text())
        .then((stationsDelete) => {
            stationsDeleteStr = stationsDelete;
        })
        .catch((err) => console.error(err));
    fetch("/templates/loading.mst").then((x) => x.text())
        .then((loading) => {
            stationsLoadingStr = loading;
        })
        .catch((err) => console.error(err));
})

function deleteData() {
    const modalWindow = document.getElementById("exampleModal");
    bootstrap.Modal.getInstance(modalWindow).hide();
    const div_main = document.getElementById("content");
    div_main.innerHTML = stationsLoadingStr;
    fetch(`/api/stations/${window.location.href.split('/')[4]}`, {
        method: "POST"
    })
    const htmlStr = Mustache.render(stationsDeleteStr);
    const mainDiv = document.getElementById("content");
    mainDiv.innerHTML = htmlStr;
}

const deleteButton = document.getElementById("button-delete-station-confirmed");
deleteButton.onclick = () => {
    deleteData();
}