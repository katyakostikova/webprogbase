const express = require('express');
const router = express.Router();
const userController = require('../controllers/users');
const metroRouter = require('./metro');

router.get("/:id", userController.getUserById);

router.get("/", userController.getUsers);

router.use('/:id/stations', metroRouter);

module.exports = router;

