const router = require('express').Router();
const stationRouter = require('./metro');
const apiRouter = require('./api_station');

router.use("/stations", stationRouter)
router.use("/api/stations", apiRouter);

module.exports = router;